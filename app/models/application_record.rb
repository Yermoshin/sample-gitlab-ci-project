# frozen_string_literal: true

# Application-wide record
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
